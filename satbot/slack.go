package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type SlackMessage struct {
	Channel     string            `json:"channel"`
	Attachments []SlackAttachment `json:"attachments"`
}

type SlackAttachment struct {
	Color      string        `json:"color"`
	AuthorName string        `json:"author_name"`
	Pretext    string        `json:"pretext"`
	Title      string        `json:"title"`
	TitleLink  string        `json:"title_link"`
	Text       string        `json:"text"`
	Fields     []*SlackField `json:"fields"`
	Footer     string        `json:"footer"`
	FooterIcon string        `json:"footer_icon"`
	Timestamp  int64         `json:"ts"`
}

type SlackField struct {
	Title string `json:"title"`
	Value string `json:"value"`
	Short bool   `json:"short"`
}

func sendSlackMessage(slackMessage *SlackMessage) error {
	slackJSON, err := json.Marshal(slackMessage)
	if err != nil {
		return fmt.Errorf("Error marshlaing slack json: %w", err)
	}
	log.Print(string(slackJSON))
	resp, err := http.Post(
		slackURL,
		"application/json",
		bytes.NewBuffer(slackJSON),
	)
	if err != nil {
		return fmt.Errorf("Error sending slack message: %w", err)
	}
	log.Printf("Slack response: %+v", resp)

	return nil
}
