package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"mime/quotedprintable"
	"net/mail"
	"regexp"
	"strings"
	"time"

	"github.com/aws/aws-lambda-go/events"
)

var (
	latLonExp = regexp.MustCompile(`Lat (?P<lat>[0-9\.-]+) Lon (?P<lon>[0-9\.-]+)`)
	replyExp  = regexp.MustCompile(`(https[\S]+)&adr`)
)

type SnsEmail struct {
	events.SimpleEmailMessage
	Content string `json:"content"`
}

type coordinates struct {
	Lat string
	Lon string
}

func parseInreachEmail(sns events.SNSEntity) (*SlackMessage, error) {
	var email SnsEmail
	err := json.Unmarshal([]byte(sns.Message), &email)
	if err != nil {
		return nil, fmt.Errorf("Error unmarshaling email: %w", err)
	}

	m, err := mail.ReadMessage(strings.NewReader(email.Content))
	if err != nil {
		return nil, fmt.Errorf("Error reading email: %w", err)
	}

	r := quotedprintable.NewReader(m.Body)
	body, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, fmt.Errorf("Error reading email body: %w", err)
	}
	textBody := string(body)

	var slackFields []*SlackField
	replyURL := getReplyUrl(&textBody)
	coordinates, err := getCoordinates(&textBody)
	if err == nil {
		slackFields = []*SlackField{
			getCoordinatesField(coordinates),
			getGoogleMapsField(coordinates),
			getReplyURLField(replyURL),
		}
	} else {
		slackFields = []*SlackField{
			getReplyURLField(replyURL),
		}
	}

	attachment := SlackAttachment{
		Color:      "#FF0000",
		AuthorName: m.Header.Get("From"),
		Pretext:    "New message from low earth orbit",
		Title:      "Garmin Inreach Message",
		TitleLink:  *replyURL,
		Text:       *getMessage(&textBody),
		Fields:     slackFields,
		Footer:     footerSignature,
		FooterIcon: "https://ca.slack-edge.com/T02EX0BD3-U02EX0BD7-1bd8196dbd60-72",
		Timestamp:  time.Now().Unix(),
	}

	return &SlackMessage{
		Attachments: []SlackAttachment{attachment},
	}, nil
}

func getMessage(email *string) *string {
	messageSplit := strings.Split(*email, "View the location or send a reply to")
	return &messageSplit[0]
}

func getReplyUrl(email *string) *string {
	match := replyExp.FindStringSubmatch(*email)
	url := string(match[0])
	return &url
}

func getCoordinates(email *string) (*coordinates, error) {
	match := latLonExp.FindStringSubmatch(*email)
	coord := &coordinates{}
	if len(match) == 0 {
		return nil, errors.New("Unable to find coordinates")
	}

	for i, name := range latLonExp.SubexpNames() {
		switch name {
		case "lat":
			coord.Lat = string(match[i])
		case "lon":
			coord.Lon = string(match[i])
		}
	}
	return coord, nil
}

func getCoordinatesField(coordinates *coordinates) *SlackField {
	return &SlackField{
		Title: "Location :camping:",
		Value: fmt.Sprintf("Lat: %s\nLon: %s", coordinates.Lat, coordinates.Lon),
		Short: true,
	}
}

func getGoogleMapsField(coordinates *coordinates) *SlackField {
	return &SlackField{
		Title: "Directions :killdozer-anim:",
		Value: fmt.Sprintf("<https://www.google.com/maps/place/%s,%s|Google maps>", coordinates.Lat, coordinates.Lon),
		Short: true,
	}
}

func getReplyURLField(replyURL *string) *SlackField {
	return &SlackField{
		Title: "View location and reply via satellite :satellite:",
		Value: fmt.Sprintf("<%s|Garmin Explore>", *replyURL),
		Short: true,
	}
}
