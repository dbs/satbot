package main

import (
	"context"
	"fmt"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

var (
	slackURL        string
	footerSignature string
)

func init() {
	slackURL = os.Getenv("SLACK_WEBHOOK_URL")
	footerSignature = os.Getenv("FOOTER_SIGNATURE")

}

func handler(ctx context.Context, snsEvent events.SNSEvent) error {
	for _, record := range snsEvent.Records {
		sns := record.SNS

		slackMessage, err := parseInreachEmail(sns)
		if err != nil {
			return fmt.Errorf("Error parsing inreach message: %w", err)
		}

		err = sendSlackMessage(slackMessage)
		if err != nil {
			return fmt.Errorf("Error sending slack message: %w", err)
		}
	}
	return nil
}

func main() {
	lambda.Start(handler)
}
